let left = 0
let timer
let screenWidth = Math.floor(window.innerWidth / 10) * 10 - 20  //刚加载时获取页面宽度 Math.floor取整数部分
let fullSlide = document.querySelector(".fullSlide")
let imgList = document.querySelector(".imgList")
let imgs = imgList.querySelectorAll("img")
let esiocList = document.querySelector(".icoList")
let esioc = esiocList.getElementsByTagName("i")       //标签名查找

// 1.添加屏幕大小变化事件监听器
window.addEventListener('resize', handleResize);

// 1.定义函数来处理屏幕大小变化
handleResize()
function handleResize() {
    // 获取屏幕宽度
    screenWidth = Math.floor(window.innerWidth / 10) * 10 - 20
    if (screenWidth < 1190) {
        screenWidth = 1190
    }
    // 给背景，ul设置宽度
    fullSlide.style.width = screenWidth + "px"
    imgList.style.width = screenWidth * 5 + "px"
    // 更改图片宽度
    imgwith()
}

// 设置图片宽度
imgwith();
function imgwith() {
    // 循环遍历每个img元素并设置新的宽度
    imgs.forEach(function (img) {
        img.style.width = screenWidth + "px";
    });
}

// 2.图片滚动
run()
function run() {
    if (left <= -screenWidth * 4) {
        left = 0    // 图片回到起始
    }
    // 向左移动left值
    imgList.style.marginLeft = left + 'px'
    // 切换一张后停留1200ms，然后10ms向左移动10px像素，变量n不能用let，
    var n = (left % screenWidth == 0) ? n = 5000 : n = 10
    left -= 10
    timer = setTimeout(run, n)  //等待事件
    // 获取当前图片对应的序号，更改圆形图标颜色，从0开始
    let m = Math.floor(-left / screenWidth)
    icochange(m)
}

// 3.实现图片更换
function imgChange(n) {
    let lt = -(n * screenWidth)
    imgList.style.marginLeft = lt + 'px'
    left = lt
}

// 4.底部小圆点背影颜色对应相应的轮播图
function icochange(m) {
    // 初始小圆点图片背景颜色
    for (let index = 0; index < esioc.length; index++) {
        esioc[index].style.backgroundColor = '';
    }
    // 修改对应li图标下的背景颜色
    if (m < esioc.length) {
        esioc[m].style.backgroundColor = 'white';
    }
}

// 5.点击小圆点实现对应图片转换
esiocList.onclick = function () {
    // 获取当前ul标签对应的li标签元素 如：<li>1</li>
    let tg = event.target
    // 获取li标签的里面的值，<li>1</li>下的’1‘，-1为对应的图片下标
    let ico = tg.innerHTML - 1
    // 由li标签的值，更改轮播图片和小圆点背景颜色
    imgChange(ico)
    icochange(ico)
}